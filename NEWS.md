# laggedcor 0.0.1

* Added a `NEWS.md` file to track changes to the package.

# laggedcor 0.99.3

* Update time_plot function.
