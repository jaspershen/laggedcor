% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/extract_data.R
\name{extract_all_cor_p}
\alias{extract_all_cor_p}
\title{extract_all_cor_p}
\usage{
extract_all_cor_p(object)
}
\arguments{
\item{object}{a lagged_scatter_result class object.}
}
\value{
A vector.
}
\description{
extract_all_cor_p
}
\examples{
data("object", package = "laggedcor")
extract_all_cor_p(object = object)
}
\author{
Xiaotao Shen
\email{shenxt1990@outlook.com}
}
