% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/extract_data.R
\name{extract_shift_time}
\alias{extract_shift_time}
\title{extract_shift_time}
\usage{
extract_shift_time(object, numeric = TRUE)
}
\arguments{
\item{object}{a lagged_scatter_result class object.}

\item{numeric}{shift time as numeric or character}
}
\value{
A vector.
}
\description{
extract_shift_time
}
\examples{
data("object", package = "laggedcor")
extract_shift_time(object = object, numeric = TRUE)
extract_shift_time(object = object, numeric = FALSE)
}
\author{
Xiaotao Shen
\email{shenxt1990@outlook.com}
}
